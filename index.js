var express  = require('express'),
	http	 = require('http'),
	dbApi 	 = require('./db-api/index'),
	engine   = require('socket.io');


var app      = express();
app.use('/', express.static(__dirname + '/public'));

app.get('/', function(req, res, next){
	res.sendFile(__dirname +'/index.html');
});

app.get('/pokemons', function(req, res, next){
	dbApi.find(function(pokemons) {
		res.json(pokemons);
	});
});

var server = http.createServer(app);
server.listen(3000, function(){
	console.log('Servidor de archivos estaticos corriendo en http://localhost:3000');	
});

var io = engine.listen(server);
io.on('connection', function(socket){
	socket.on('message', function(msg){
		io.emit('message', msg);
	});
});