//PokeApp
var React = require('react');
var PokeTable = require('./PokeTable');
var PokeChat = require('./PokeChat');
var $ = require('jquery');
var io = require('socket.io-client');
var uid = require('uid');

var PokeApp = React.createClass({
	getInitialState: function(){
		return { 
			messages: [],
			pokemons: [],
			onGrowl: this.onGrowl.bind(this),			
			user: uid(10) 
		}		
	},
	onGrowl: function(name){		
		var text = name +', '+ name +'!';
		var message = { id: uid(), text: text, user: this.state.user };
		this.newMessage(message);
		this.socket.emit('message', message);
	},
	newMessage: function(message){
		this.state.messages.push(message);
		var messages = this.state.messages;
		this.setState({ messages: messages });
	},
	componentWillMount: function(){
		$.get('/pokemons', function(pokemons){
			console.log(pokemons);
			this.setState({ pokemons: pokemons});
		}.bind(this));
		this.socket = io('http://localhost:3000');
		this.socket.on('message', function(message){
			if(this.state.user !== message.user){
				this.newMessage(message);
			}
		}.bind(this));
	},
    render: function(){
		// var pokemons = [
		//     { number: 1 , name: 'Bulbasaur'},
		//     { number: 2 , name: 'Ivysaur'},
		//     { number: 3 , name: 'Venasaur'}
		// ];
		if( this.state.pokemons.length ){
        return (<div className="pokeapp">
        			<PokeTable 
        				pokemons = { this.state.pokemons } 
        				onGrowl  = { this.onGrowl } />
        			<PokeChat 
        				messages = { this.state.messages } />
        		</div>);
		}else{
			return (<p>cargando ...</p>);
		}
    }
});

module.exports = PokeApp;