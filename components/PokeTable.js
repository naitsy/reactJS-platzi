var React = require('react');
var PokeRow = require('./PokeRow');

var PokeTable = React.createClass({	
    render: function(){
   //  	var rows = [];
   //  	this.props.pokemons.forEach(function(poke){
			// rows.push(<PokeRow key   = { poke.number } 
   //                  		name  = { poke.name } 
   //                  		number= { poke.number } 
   //                  		growl = { this.props.onGrowl } />)
   //  	}.bind(this));
        return <ul className="poketable">
                { 
                	this.props.pokemons.map(function(pokemon){                	
	                    return <PokeRow key   = { pokemon.number } 
	                    				name  = { pokemon.name } 
	                    				number= { pokemon.number } 
	                    				growl = { this.props.onGrowl } />
                    }.bind(this))
                }
        		</ul>
    }
});


module.exports = PokeTable;