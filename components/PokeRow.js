var React = require('react');
var PokeAvatar = require('./PokeAvatar');
var PokeChat = require('./PokeChat');
var PokeMessage = require('./PokeMessage');

var PokeRow = React.createClass({
	onClick: function(ev){
		this.props.growl.call(null, this.props.name);
	},
    render: function(){    
        return <li className="pokerow" onClick={ this.onClick.bind(this) }>
                <PokeAvatar 
                	number = { this.props.number } />
                		{ this.props.name }
               </li>                
    }
});

module.exports = PokeRow;