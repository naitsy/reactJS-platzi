var React = require('react');
var PokeAvatar = React.createClass({
    render: function(){
        var url = 'http://veekun.com/dex/media/pokemon/main-sprites/x-y/'+ this.props.number +'.png';
        return <img src={ url } className="avatar" />
    }
});

module.exports = PokeAvatar;