var React = require('react/addons');
var PokeMessage = require('./PokeMessage');

var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

var PokeChat = React.createClass({
    render: function(){
        return(<ul className="pokechat">
        			<ReactCSSTransitionGroup transitionName="message-animation">
        			{ this.props.messages.map(function(message){
        										return <PokeMessage message={ message } />
        									}) }
        			</ReactCSSTransitionGroup>
        	   </ul>
       	);
    }
});

module.exports = PokeChat;