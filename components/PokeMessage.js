var React = require('react');

var PokeMessage = React.createClass({
    render: function(){                    
        return <li className="pokemessage" key={ this.props.id }>
        			{ this.props.message.text }
        	   </li>
    }
});

module.exports = PokeMessage;