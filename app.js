var React = require('react');
var PokeApp = require('./components/PokeApp');

React.render(
    <PokeApp />, 
    document.getElementById('container')
);
